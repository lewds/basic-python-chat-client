from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread

Clients = {}
Addresses = {}

HOST = ''
PORT = 6667
BUFSIZ = 1024
ADDR = (HOST, PORT)

ServerSocket = socket(AF_INET, SOCK_STREAM)
ServerSocket.bind(ADDR)

def MessageAllClients(msg, prefix=""):
    for ClientSocket in Clients:
        ClientSocket.send(bytes(prefix, "utf8") + msg)


def HandleClient(Client):
    name = Client.recv(BUFSIZ).decode("utf8")
    Client.send(bytes("Nice going, %s." %name, "utf8"))
    msg = "%s has joined." % name
    MessageAllClients(bytes(msg, "utf8"))
    Clients[Client] = name

    while True:
        msg = Client.recv(BUFSIZ)
        if msg != bytes("!quit", "utf8"):
            MessageAllClients(msg, name + ": ")
        else:
            Client.send(bytes("!quit", "utf8"))
            Client.close()
            del Clients[Client]
            MessageAllClients(bytes("%s has left the chat." % name, "utf8"))
            break


def AcceptConnections():
    #Sets up handling for incoming clients.
    while True:
        Client, Client_Address = ServerSocket.accept()
        print("%s:%s has connected." % Client_Address)
        Client.send(bytes("Alright boy, give us 'yer name.", "utf8"))
        Addresses[Client] = Client_Address
        Thread(target=HandleClient, args=(Client,)).start()


ServerSocket.listen(5)
print("Waiting for connection...")

ClientThread = Thread(target=AcceptConnections())
ClientThread.start()
ClientThread.join()

ServerSocket.close()
